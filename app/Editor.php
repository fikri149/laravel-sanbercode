<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editor extends Model
{
    protected $fillable = [
        'nama', 'email', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
